import React, { useEffect, useState } from 'react';
import { AppBar, Container, Toolbar, Typography } from '@material-ui/core';
import { VideosIndex } from './components/videos-index';
import { addVideo, deleteVideo, getVideos, updateVideo } from './services/videos';
import { Author, Category, VideoFormData, ProcessedVideo } from './common/interfaces';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Router } from 'react-router';
import { VideosAdd } from './components/videos-add';
import { getAuthors } from './services/authors';
import { getCategories } from './services/categories';
import { appHistory } from './common/browser-history';
import { VideosEdit } from './components/videos-edit';

const App: React.FC = () => {
  const [videos, setVideos] = useState<ProcessedVideo[]>([]);
  const [authors, setAuthors] = useState<Author[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);

  // TODO: show "spinner"
  // TODO: show errors

  useEffect(() => {
    getVideos().then(setVideos);
    // TODO: Consider getting authors and categories only but passing them to some mapper to get videos
    getAuthors().then(setAuthors);
    getCategories().then(setCategories);
  }, []);

  async function onDeleteVideoSubmit(videoId: number) {
    const updatedVideos = await deleteVideo(videoId);

    setVideos(updatedVideos);
  }

  async function onAddVideoSubmit(data: VideoFormData) {
    const updatedVideos = await addVideo(data);

    setVideos(updatedVideos);
    goBackToVideosIndex();
  }

  async function onEditVideoSubmit(videoId: number, data: VideoFormData) {
    const updatedVideos = await updateVideo(videoId, data);

    setVideos(updatedVideos);
    goBackToVideosIndex();
  }

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">Videos</Typography>
        </Toolbar>
      </AppBar>
      <Container style={{ marginBottom: 40 }}>
        <Router history={appHistory}>
          <Switch>
            <Route path="/" exact>
              <Redirect to="/videos" />
            </Route>
            <Route path="/videos/add">
              <VideosAdd onCancel={onAddVideoCancel} onSubmit={onAddVideoSubmit} authors={authors} categories={categories} />
            </Route>
            <Route path="/videos/:id">
              <VideosEdit onCancel={onAddVideoCancel} onSubmit={onEditVideoSubmit} authors={authors} categories={categories} />
            </Route>
            <Route path="/videos">
              <VideosIndex
                videos={videos}
                onDeleteVideoClick={onDeleteVideoSubmit}
                onAddVideoClick={onAddVideoClick}
                onEditVideoClick={onEditVideoClick}
              />
            </Route>
          </Switch>
        </Router>
      </Container>
    </>
  );
};

export default App;

function onAddVideoClick() {
  appHistory.push('/videos/add');
}

function onEditVideoClick(videoId: number) {
  appHistory.push('/videos/' + videoId);
}

function onAddVideoCancel() {
  goBackToVideosIndex();
}

function goBackToVideosIndex() {
  if (appHistory.length > 0) {
    return appHistory.goBack();
  }

  appHistory.replace('/videos');
}
