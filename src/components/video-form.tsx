import {
  Button,
  Select,
  TextField,
  MenuItem,
  FormControl,
  InputLabel,
  makeStyles,
  Checkbox,
  FormControlLabel,
  FormGroup,
  FormLabel,
} from '@material-ui/core';
import React, { ChangeEvent, FormEvent, useState } from 'react';
import { Author, Category, VideoFormData } from '../common/interfaces';

interface Props {
  onSubmit: (data: VideoFormData) => void;
  onCancel: () => void;
  authors: Author[];
  categories: Category[];
  initialState: VideoFormData;
  title: string;
}

export const VideoForm: React.FC<Props> = ({ authors, categories, onSubmit, onCancel, initialState, title }) => {
  const [data, setData] = useState<VideoFormData>(initialState);

  const classes = useStyles();

  function onFormSubmit(event: FormEvent) {
    event.preventDefault();

    // TODO: Add own validation

    onSubmit(data);
  }

  function onNameChange(event: ChangeEvent<HTMLInputElement>) {
    setData({
      ...data,
      name: event.target.value,
    });
  }

  function onAuthorChange(event: ChangeEvent<{ value: any }>) {
    setData({
      ...data,
      authorId: event.target.value ? String(event.target.value) : '',
    });
  }

  function onCategoryToggle(event: ChangeEvent<HTMLInputElement>) {
    const checked = event.target.checked;
    const categoryId = event.target.name;

    if (checked) {
      setData({
        ...data,
        catIds: [...data.catIds, categoryId],
      });

      return;
    }

    setData({
      ...data,
      catIds: data.catIds.filter((id) => id !== categoryId),
    });
  }

  return (
    <div>
      <div className={classes.header}>
        <h3 className={classes.title}>{title}</h3>
        <Button variant="outlined" color="secondary" onClick={onCancel}>
          Cancel
        </Button>
      </div>
      <form className={classes.form} onSubmit={onFormSubmit}>
        <TextField className={classes.formControl} label="Name" required onChange={onNameChange} value={data.name} />
        <FormControl className={classes.formControl} required>
          <InputLabel>Author</InputLabel>
          <Select value={data.authorId} onChange={onAuthorChange}>
            {authors.map((aut) => (
              <MenuItem key={aut.id} value={aut.id}>
                {aut.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl component="fieldset" className={classes.formControl} required>
          <FormLabel component="legend">Categories</FormLabel>
          <FormGroup>
            {categories.map((cat) => {
              const categoryId = String(cat.id);

              return (
                <FormControlLabel
                  key={categoryId}
                  control={<Checkbox checked={data.catIds.includes(categoryId)} onChange={onCategoryToggle} name={categoryId} />}
                  label={cat.name}
                />
              );
            })}
          </FormGroup>
        </FormControl>
        <div style={{ margin: '40px 0' }}>
          <Button type="submit" variant="outlined" color="primary">
            Confirm
          </Button>
        </div>
      </form>
    </div>
  );
};

const useStyles = makeStyles((theme) => ({
  title: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: theme.spacing(1),
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  formControl: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));
