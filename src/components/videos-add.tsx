import React from 'react';
import { Author, Category, VideoFormData } from '../common/interfaces';
import { VideoForm } from './video-form';

interface Props {
  onSubmit: (data: VideoFormData) => void;
  onCancel: () => void;
  authors: Author[];
  categories: Category[];
}

const INITIAL_STATE: VideoFormData = {
  name: '',
  catIds: [],
  authorId: '',
};

export const VideosAdd: React.FC<Props> = (props) => {
  return <VideoForm initialState={INITIAL_STATE} title="Add video" {...props} />;
};
