import { Button, makeStyles } from '@material-ui/core';
import React, { useState } from 'react';
import { ProcessedVideo } from '../common/interfaces';
import { SearchField } from './search-field';
import { VideosTable } from './videos-table';

interface Props {
  videos: ProcessedVideo[];
  onDeleteVideoClick: (videoId: number) => void;
  onEditVideoClick: (videoId: number) => void;
  onAddVideoClick: () => void;
}

export const VideosIndex: React.FC<Props> = ({ videos, onDeleteVideoClick, onAddVideoClick, onEditVideoClick }) => {
  const [searchWords, setSearchWords] = useState<string[]>([]);

  const classes = useStyles();

  function onSearchQueryChange(searchQuery: string) {
    setSearchWords(getSearchWords(searchQuery));
  }

  function onDeleteClick(video: ProcessedVideo) {
    const confirmed = window.confirm(`Video "${video.name}" will be removed. This operation cannot be undone. Please confirm.`);

    if (!confirmed) {
      return;
    }

    onDeleteVideoClick(video.id);
  }

  return (
    <div>
      <div className={classes.header}>
        <SearchField onChange={onSearchQueryChange} />
        <Button variant="outlined" color="primary" onClick={onAddVideoClick}>
          Add video
        </Button>
      </div>
      <VideosTable videos={filterVideos(searchWords, videos)} onDeleteClick={onDeleteClick} onEditClick={onEditVideoClick} />
    </div>
  );
};

const whitespaceRe = /\s+/gim;

function getSearchWords(searchQuery: string): string[] {
  const query = searchQuery.trim().toLowerCase();

  if (!query.length) {
    return [];
  }

  return query.split(whitespaceRe);
}

function filterVideos(searchWords: string[], videos: ProcessedVideo[]): ProcessedVideo[] {
  if (!searchWords.length) {
    return videos;
  }

  return videos.filter((vid) => {
    for (const word of searchWords) {
      if (vid.name.toLowerCase().includes(word)) {
        return true;
      }

      if (vid.author.toLowerCase().includes(word)) {
        return true;
      }

      if (vid.bestFormat.toLowerCase().includes(word)) {
        return true;
      }

      if (vid.categories.join(' ').toLowerCase().includes(word)) {
        return true;
      }
    }

    return false;
  });
}

const useStyles = makeStyles((theme) => ({
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: theme.spacing(1),
  },
}));
