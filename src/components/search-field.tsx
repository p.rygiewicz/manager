import { TextField } from '@material-ui/core';
import React, { ChangeEvent } from 'react';

interface Props {
  onChange: (searchQuery: string) => void;
}

export class SearchField extends React.PureComponent<Props> {
  render() {
    return <TextField id="videos-search" label="Search videos" type="search" onChange={this.onSearchQueryChange} />;
  }

  inputTimeout: any = null;

  onSearchQueryChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;

    clearTimeout(this.inputTimeout);
    // throttle input changes
    this.inputTimeout = setTimeout(() => this.props.onChange(value), 400);
  };
}
