import React from 'react';
import { Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { ProcessedVideo } from '../common/interfaces';

interface VideosTableProps {
  videos: ProcessedVideo[];
  onDeleteClick: (video: ProcessedVideo) => void;
  onEditClick: (videoId: number) => void;
}

export const VideosTable: React.FC<VideosTableProps> = ({ videos, onDeleteClick, onEditClick }) => {
  return (
    <TableContainer component={Paper} style={{ marginTop: '40px' }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Video Name</TableCell>
            <TableCell>Author</TableCell>
            <TableCell>Categories</TableCell>
            <TableCell>Highest quality format</TableCell>
            <TableCell>Release date</TableCell>
            <TableCell>Options</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {videos.map((video) => (
            <TableRow key={video.id}>
              <TableCell component="th" scope="row">
                {video.name}
              </TableCell>
              <TableCell>{video.author}</TableCell>
              <TableCell>{video.categories.join(', ')}</TableCell>
              <TableCell>{video.bestFormat}</TableCell>
              <TableCell>{formatDate(video.releaseDate)}</TableCell>
              <TableCell>
                <Button color="primary" variant="contained" onClick={() => onEditClick(video.id)}>
                  Edit
                </Button>
                <Button color="secondary" variant="contained" style={{ marginLeft: 8 }} onClick={() => onDeleteClick(video)}>
                  Delete
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

function formatDate(date: Date): string {
  return date.toISOString().slice(0, 10);
}
