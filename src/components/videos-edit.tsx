import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Author, Category, VideoFormData } from '../common/interfaces';
import { getVideoFormData } from '../services/videos';
import { VideoForm } from './video-form';

interface Props {
  onSubmit: (videoId: number, data: VideoFormData) => void;
  onCancel: () => void;
  authors: Author[];
  categories: Category[];
}

export const VideosEdit: React.FC<Props> = ({ onSubmit, ...props }) => {
  const [initialData, setInitialData] = useState<VideoFormData | null>(null);

  const params = useParams<any>();
  const videoId = parseInt(params.id);

  useEffect(() => {
    if (isNaN(videoId)) {
      return; // TODO: Show error
    }

    // TODO: Catch and show error
    getVideoFormData(videoId).then(setInitialData);
  }, [videoId]);

  if (!initialData) {
    return null;
  }

  function onVideoSubmit(data: VideoFormData) {
    onSubmit(videoId, data);
  }

  return <VideoForm initialState={initialData} title={`Edit video: ${initialData.name}`} onSubmit={onVideoSubmit} {...props} />;
};
