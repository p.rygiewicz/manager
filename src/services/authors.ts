import { Author } from '../common/interfaces';

export const getAuthors = (): Promise<Author[]> => {
  return fetch(`${process.env.REACT_APP_API}/authors`).then((response) => response.json() as unknown as Author[]);
};

export const replaceAuthor = (authorId: number, data: Author) => {
  return fetch(`${process.env.REACT_APP_API}/authors/${authorId}`, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
};
