import { getCategories } from './categories';
import { replaceAuthor, getAuthors } from './authors';
import { Author, Category, NamedVideoFormat, VideoFormData, ProcessedVideo, Video } from '../common/interfaces';

// TODO: Consider using PATCH
export const updateVideo = async (videoId: number, data: VideoFormData): Promise<ProcessedVideo[]> => {
  const authors = await getAuthors();

  let existingVideo: Video | undefined;
  let currentAuthor = authors.find((aut) => {
    const video = aut.videos.find((vid) => vid.id === videoId);

    if (video) {
      existingVideo = video;
    }

    return !!video;
  });

  if (!currentAuthor || !existingVideo) {
    throw new Error('Existing author or video not found');
  }

  if (String(currentAuthor.id) === data.authorId) {
    // update existing author
    currentAuthor = {
      ...currentAuthor,
      videos: currentAuthor.videos.map((vid) => {
        if (vid.id !== videoId) {
          return vid;
        }

        return {
          ...vid,
          name: data.name,
          catIds: data.catIds.map((id) => parseInt(id)),
        };
      }),
    };

    // TODO: Check http response code
    await replaceAuthor(currentAuthor.id, currentAuthor);
  } else {
    // move updated video to different author
    let targetAuthor = authors.find((aut) => String(aut.id) === data.authorId);

    if (!targetAuthor) {
      throw new Error('Target author not found');
    }

    targetAuthor = {
      ...targetAuthor,
      videos: [
        ...targetAuthor.videos,
        {
          ...existingVideo,
          name: data.name,
          catIds: data.catIds.map((id) => parseInt(id)),
        },
      ],
    };

    currentAuthor = {
      ...currentAuthor,
      videos: currentAuthor.videos.filter((vid) => vid.id !== videoId),
    };

    // TODO: Check http response code
    await replaceAuthor(targetAuthor.id, targetAuthor);
    await replaceAuthor(currentAuthor.id, currentAuthor);
    // TODO: Fix api (no safe/atomic way to change author)
  }

  return getVideos();
};

export const addVideo = async (data: VideoFormData): Promise<ProcessedVideo[]> => {
  // TODO: Fix api (POST video not available)
  const authors = await getAuthors();

  let author = authors.find((aut) => String(aut.id) === data.authorId);

  if (!author) {
    throw new Error('Author not found');
  }

  const video: Video = {
    id: 0,
    name: data.name,
    catIds: data.catIds.map((id) => parseInt(id)),
    formats: {},
  };

  author = {
    ...author,
    videos: [...author.videos, video],
  };

  // TODO: Check http response code
  await replaceAuthor(author.id, author);

  return getVideos();
};

export const deleteVideo = async (videoId: number): Promise<ProcessedVideo[]> => {
  // TODO: Fix api (DELETE videoId not available)
  const authors = await getAuthors();

  let author = authors.find((aut) => {
    const hasVideo = !!aut.videos.find((vid) => vid.id === videoId);

    return hasVideo;
  });

  if (!author) {
    throw new Error('Video not found');
  }

  author = {
    ...author,
    videos: author.videos.filter((vid) => vid.id !== videoId),
  };

  // TODO: Check http response code
  await replaceAuthor(author.id, author);

  return getVideos();
};

export const getVideoFormData = async (videoId: number): Promise<VideoFormData> => {
  const authors = await getAuthors();
  const formData = mapVideoFormData(authors, videoId);

  if (!formData) {
    throw new Error('Video not found');
  }

  return formData;
};

export const getVideos = async (): Promise<ProcessedVideo[]> => {
  const [categories, authors] = await Promise.all([getCategories(), getAuthors()]);

  return mapVideoList(categories, authors);
};

function mapVideoFormData(authors: Author[], videoId: number): VideoFormData | null {
  let result: VideoFormData | null = null;

  for (const aut of authors) {
    const video = aut.videos.find((vid) => vid.id === videoId);

    if (video) {
      result = {
        name: video.name,
        authorId: String(aut.id),
        catIds: video.catIds.map((id) => String(id)),
      };

      break;
    }
  }

  return result;
}

function mapVideoList(categories: Category[], authors: Author[]): ProcessedVideo[] {
  const result: ProcessedVideo[] = [];

  authors.forEach((aut) => {
    aut.videos.forEach((vid) => {
      result.push({
        id: vid.id,
        name: vid.name,
        author: aut.name,
        categories: mapCategoryNameList(vid.catIds, categories),
        bestFormat: getBestFormatLabel(vid),
        releaseDate: getRandomDate(),
      });
    });
  });

  return result;
}

function mapCategoryNameList(catIds: number[], categories: Category[]): string[] {
  return categories.filter((cat) => catIds.includes(cat.id)).map((cat) => cat.name);
}

function getBestFormatLabel(video: Video): string {
  const shortlist: NamedVideoFormat[] = [];
  let maxSize = 0;

  Object.entries(video.formats).forEach(([name, format]) => {
    if (format.size >= maxSize) {
      shortlist.push({
        ...format,
        name,
      });
    }
  });

  if (shortlist.length === 0) {
    return '';
  }

  const bestFormat = shortlist.reduce((acc, format) => {
    if (parseInt(format.res) > parseInt(acc.res)) {
      return format;
    }

    return acc;
  });

  return `${bestFormat.name} ${bestFormat.res}`;
}

const today = new Date();

function getRandomDate(): Date {
  return new Date(Math.round(today.getTime() * Math.random()));
}
