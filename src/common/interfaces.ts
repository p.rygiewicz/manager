interface VideoFormatMap {
  [name: string]: {
    res: string;
    size: number;
  };
}

export interface NamedVideoFormat {
  res: string;
  size: number;
  name: string;
}

export interface Category {
  id: number;
  name: string;
}

export interface Video {
  id: number;
  catIds: number[];
  name: string;
  formats: VideoFormatMap;
}

export interface VideoFormData {
  catIds: string[];
  name: string;
  authorId: string;
}

export interface Author {
  id: number;
  name: string;
  videos: Video[];
}

export interface ProcessedVideo {
  id: number;
  name: string;
  author: string;
  categories: string[];
  bestFormat: string;
  releaseDate: Date;
}
